# MicrobeLog Manifest

This is a project done by me, Claes Wallin
<claes.wallin@greatsinodevelopment.com>.

The official home of the project is https://gitlab.com/groups/microbelog and the official home of this manifest is https://gitlab.com/microbelog/manifest .

The subject of the project is a federated social platform, a
microblogging platform, written in Python. The goals are to support
(as federated node or as client):

1. the emerging W3C Social protocol
2. the pump.io protocol
3. OStatus
4. IndieWeb protocols
5. e-mail
6. whatever RSS/ATOM/PuSH feed
7. Twitter
8. Diaspora
9. Facebook

The design priorities are:

1. public post search engine indexability and discoverability
2. progressive enhancement, function fully (but maybe not as neatly)
   without javascript enabled

The aim of this project is to explore and learn about:

 - [Pyramid](http://www.pylonsproject.org/projects/pyramid/)
 - [XUDD](https://github.com/xudd/xudd)
 - Probably [Celery](http://www.celeryproject.org/)
 - Microservices, in the [Richard Rodger](https://www.youtube.com/watch?v=fVfWuked2qE) sense
 - Triple/quad stores
 - [JSON-LD](http://json-ld.org/)
 - [Docker](https://www.docker.com/)
 - The [W3C Social WG work](http://www.w3.org/Social/WG)
 - General webbery and software craft

The fallout from this project may be, if we're really lucky:

 - Useful Python packages for doing
   - JSON-LD stuff
   - Semweb stuff
   - Message bussing
   - Microservery
   - Whatever shows up on the way

The spec may look ambitious, but don't take it too seriously. I
believe that in order to do things and therefore learn things, you
need a goal. In the course of looking for that goal you discover
things and learn things that you never thought of in the first place
and you come out a better person. And that's fine too.

In short, in this project, yak-shaving is not detracting from the real
goal. Yak-shaving is the real goal. The project itself is an excuse
that drives the yak, keeps it moving instead of sleeping.

I am doing this in my spare time that is not occupied with work or
taking care of my little boy. This means basically weekday mornings
while having breakfast.

Things that are unlikely to come from this project (not for lack of trying):

 - An actual working product, even minimally viable
 - Awesome experiments juggling different languages for different
   microservices, just for the heck of it, in particular:
    - node.js
	- Haskell
	- OCaml
	- Erlang

This project is licensed under
[copyleft-next 0.3.0](https://raw.githubusercontent.com/richardfontana/copyleft-next/master/Releases/copyleft-next-0.3.0),
but with a Copyleft Sunset of five years. To be clear:

The conditions in sections 2 through 5 of copyleft-next 0.3.0 no
longer apply once five years have elapsed from the date of My first
Distribution of My Work under that License.

I'm always up for a good licensing discussion. Feel free to contact me
if you think I'm going about this all wrong.

I will accept patches and pull requests to this project if they
further the ostensible goal of producing a federated social node
written in Python, while at the same time fulfilling the actual goal
of teaching me something about the auxiliary concerns. Shave my yaks
and show me how and why.

By appending the line `Signed-off-by: Your Name
<your.name@yourhost.com> (gitlab: youruser)` to your commit message,
you affirm that you are abiding by the
[Developer's Certificate of Origin 1.1](http://developercertificate.org/). In
short, you own the copyright to or have otherwise received the right
to distribute your contribution, and you provide your contribution
under the license of the project.
